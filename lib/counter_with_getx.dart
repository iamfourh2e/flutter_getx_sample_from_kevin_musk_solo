import 'package:get/get.dart';

class CounterControllerWithX extends GetxController{
    var count = 0.obs;
    increment() => count++;
}