import 'package:flutter/material.dart';
import 'package:fluttersample/app_state.dart';
import 'package:fluttersample/counter_with_getx.dart';
import 'package:get/get.dart';
import 'package:velocity_x/velocity_x.dart';

class SecondPage extends StatefulWidget {
  @override
  _SecondPageState createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {
  final CounterControllerWithX c = Get.find();

  final appC = Get.find<AppStateController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Obx(() => "${appC.cart.length}".text.size(20).bold.make()),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          appC.addItemToCart("1");
        },
      ),
      body: "${c.count}".text.make().centered(),
    );
  }
}
