import 'dart:async';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttersample/app_state.dart';
import 'package:fluttersample/counter_with_getx.dart';
import 'package:fluttersample/second_page.dart';
import 'package:get/get.dart';
import 'package:velocity_x/velocity_x.dart';
import 'counter_controller.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String title = "";
  CounterStream counterStream;
  final CounterControllerWithX c = Get.put(CounterControllerWithX());
  final AppStateController appC = Get.put(AppStateController());

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () => c.increment(),
          child: Icon(Icons.add),
        ),
        appBar: AppBar(
          title: Obx(() => "${appC.cart.length}".text.make()),
          actions: [
            IconButton(
                icon: Icon(Icons.arrow_right),
                onPressed: () => Get.to(SecondPage()))
          ],
        ),
        body: Obx(() => "${c.count}".text.size(40).bold.make().centered()));
  }
}

//stream , observer dependency change
//local state, global state
// cart -* orderLineItem
// orderLineItem -> 1 total
