import 'package:rxdart/rxdart.dart';

class CounterStream {
  CounterStream();
  int _counter = 0;

  addCounter(val) {
    _counter += val;
    subjectCounter.add(_counter);
  }
  var subjectCounter = BehaviorSubject<int>();

  dispose() {
    subjectCounter.close();
  }
}
